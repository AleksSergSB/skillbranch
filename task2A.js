const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors());
app.get('/', (req, res) => {
	res.send('сумма двух чисел A и B. <br> Отправлять через URL \/summ?')
});

app.get('/summ', (req, res) => {
	const summ = (+req.query.a || 0) + (+req.query.b || 0);
	//res.send('a: ' +req.query.a+ "  b: " +req.query.b+ '<br>');
	//if (isNaN(summ)) res.send('0')
	//	else 	
			res.send(String(summ));
});


app.listen(3000, () => {
	console.log('Слушаем порт 3000');
})