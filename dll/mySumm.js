module.exports =  (a, b, ...other/*{accuracy}*/) => {
 let accuracy;
 if (isNaN(other[0])) accuracy=2
  else accuracy=other[0]; 

	return (a + b).toFixed(accuracy)
}

// Суммирование 'a' 'b', с точностью ['accuracy']
// [accuracy] to default = 2