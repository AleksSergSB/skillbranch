const express = require('express');
const fetch = require('isomorphic-fetch');
const Promises = require ('bluebird');
const cors = require('cors');

const app = express();
app.use(cors());
// BaseInfoSJ = '{"board":{"vendor":"IBM","model":"IBM-PC S-100","cpu":{"model":"80286","hz":12000},"image":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg","video":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4"},"ram":{"vendor":"CTS","volume":1048576,"pins":30},"os":"MS-DOS 1.25","floppy":0,"hdd":[{"vendor":"Samsung","size":33554432,"volume":"C:"},{"vendor":"Maxtor","size":16777216,"volume":"D:"},{"vendor":"Maxtor","size":8388608,"volume":"C:"}],"monitor":null,"length":42,"height":21,"width":54}';
// var BB = JSON.parse (BaseInfoSJ);

const BaseU = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json'

function jsonSS(json1, json2){
    var out = {};
    for(var k1 in json1){
        if (json1.hasOwnProperty(k1)) out[k1] = json1[k1];
    }
    for(var k2 in json2){
        if (json2.hasOwnProperty(k2)) {
            if(!out.hasOwnProperty(k2)) out[k2] = json2[k2];
            else if(
                (typeof out[k2] === 'object') && (out[k2].constructor === Object) && 
                (typeof json2[k2] === 'object') && (json2[k2].constructor === Object)
            ) out[k2] = json_merge_recursive(out[k2], json2[k2]);
        }
    }
    return out;
}



function getMainInfo (url) {
  return fetch(url).then((res) => res.json());
}

async function masterHandler(taken) {
	
	if (taken[taken.length-1] == "/"){ // Костыль. Лучше через регулярку переделать!!
		taken =taken.slice(0,-1);
	}
	const taken1 = await /(\/\w+)($|\/\w+)($|\/\w+)/.exec(taken);
	return taken1;
}

app.get('/', async (req,res) =>  {
  const BaseInfo = await getMainInfo(BaseU);	
  return res.status(200).send(BaseInfo);
});

function myVolumes(objec){
	let objects = {};
	const vd = objec.map( (elem) => {
		if (typeof objects[elem.volume] != 'undefined') 
			return objects[elem.volume] = objects[elem.volume] +elem.size
			else 
				return objects[elem.volume] = +elem.size;
	})
	for (let key in objects) {
		objects[key] = objects[key]+'B';
	}
	return JSON.stringify(objects);
}

app.get('/*', async (req,res) =>  {
		console.log('--------------------------------------------------------');
		console.log(`запрос Get: ${req.url}`);


	const BaseInfo = await getMainInfo(BaseU);
	
    if (req.url.toLowerCase() == '/volumes')    	
    	return res.status(200).send(myVolumes(BaseInfo.hdd))
    
    const hand = await masterHandler(req.url);  
    if (hand == null)
    	return res.status(404).send('Not Found');	

    let StrGet='BaseInfo.'+(/\w+/.exec(hand[1]))[0].toLowerCase();
    for (let i =2; i<4; i++ ){ // глубина 2. можно увеличить для масштабируемости!
       if (hand[i] != ""){
    	if (isFinite(/\w+/.exec(hand[i])) ) {
    		    StrGet = StrGet+'['+(/\w+/.exec(hand[i]))[0]+']'
    		}
    		else if (typeof hand[i] == "string" ){	
    			StrGet = StrGet+'.'+(/\w+/.exec(hand[i]))[0].toLowerCase()
    		}
       }
    	
    	  		 
    }
    
     console.log('Получилась такая строка ЗАПРОСА: '+StrGet);
    
if (!/[0-9]/.test(/(\S+)(\.\S+$)/.exec(StrGet)[2])){ 
let bb = 1;
if (/(\S+)(\.\w+$)/.exec(StrGet)[2] == '.length') {
	let testj = StrGet.substr(0,StrGet.length-7);
	testj = eval(testj);
	for (let key in testj ) {
		//console.log('Объект: '+key);
		if (key == "length") 
 	 return res.status(200).send(String(eval(StrGet)));	 
	} 
return res.status(404).send('Not Found')
}}
    	 var rezul; // ппц постыль. Поискать иное решение!!!
    	 try{
    	 rezul = eval(StrGet);	 	
    	 }catch(err) {
    	 }
    	  	 
    if (typeof rezul != 'undefined'){
    	if (isFinite(rezul)) rezul = String(rezul)
    		else if (typeof rezul != 'object') 
    			 rezul = '"'+rezul+'"';
    	return res.status(200).send(rezul)
    }     
      else return  res.status(404).send('Not Found');
});


app.listen(3000, function () {
	console.log('Слушаем порт 3000');
})

